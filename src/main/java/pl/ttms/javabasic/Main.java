package pl.ttms.javabasic;

import com.sun.org.apache.xpath.internal.operations.Mod;
import modificators.Modificator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static char[] newCharArray;

    public static Map<Integer,String> translate;
    public static List<Integer> myList;

    public static void main(String[] args) {

        System.out.println("Hello Maciej ");

        boolean aBoolean = false;

        System.out.println(aBoolean);
        aBoolean = true;
        System.out.println(aBoolean);

        byte aByte = 1;
        short aShort = 2;
        int aInt = 2147483647;
        float basia = 1.23f;
        double marcin = 12.34;
        char charOne = 'a';
        char charTwo = (1+48);

        System.out.println(charOne);
        System.out.println(charTwo);

        aInt = aInt +1;
        System.out.println(aInt);

        char[] chars = {'a','b'};

        System.out.println(chars);

        newCharArray = new char[10];
        newCharArray[0] ='a';

        print(newCharArray);


        Modificator modificator = new Modificator();

        print(newCharArray);
        translate = new HashMap();

        translate.put(1,"jest super");
        translate.put(2,"naucz sie Jenkins'a");

        System.out.println(translate.get(1));


        myList = new ArrayList<Integer>(1);

        myList.add(1);

        System.out.println(myList.get(0));
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(2);
        myList.add(3);
        System.out.println(myList.get(9));

        System.out.println(myList);

        System.out.println(modificator.intOne);
        System.out.println(modificator.myString);
        System.out.println(modificator.myBoolean);

        Modificator mod1 = new Modificator();
        Modificator mod2 = new Modificator("TTMS");

        System.out.println(mod1.intOne);
        System.out.println(mod1.myString);
        System.out.println(mod1.myBoolean);

        System.out.println(mod2.intOne);
        System.out.println(mod2.myString);
        System.out.println(mod2.myBoolean);
        System.out.println(mod2.decorate());



    }


    public static boolean print(char[] chars){
        int counter = 0;
//        while (counter<10){
//            System.out.println(chars);
//            counter++;
//        }
//
//        counter = 0;
        do {
            System.out.println("Przerywnik");
            counter++;
        } while (counter<10);

        for (int i=0;i<15;i++){
            System.out.println(chars);
        }
        return true;
    }

}
