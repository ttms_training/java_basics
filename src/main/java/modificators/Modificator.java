package modificators;

public class Modificator {

    public boolean myBoolean;
    public int intOne;
    protected int intTwo;
    int intThree;
    private int intFour;
    public String myString;

    public Modificator() {
        myBoolean=true;
    }

    public Modificator(String myString) {
        this.myString = myString +" jest super";
    }


    public String decorate(){
        return "** "+myString+" **";
    }

}
