#Java basic

Uwaga! Jeśli chcesz wypchnć swój kod do tego repozytorium utwórz w strukturze /src/main/java katalog z swojego loginu domenowego np "brzozowskim" i tam umieść swój kod lub (lepiej) utwórz własny branch i w nim umieszczaj swoje zmiany.
Jeśli rozwizujesz któreś z zadań umieść je w katalogu nazwanym np. Task_1 lub podobnie.

##Ćwiczenia:

1. Napisz funkcję zmieniającą tempaturę Z stopni Celsjusza na Kelwiny którs będzie przyjmowała temperaturę jako parametr i zwracała wynik.
Napisz funkcję która będzie sprawdzała czy podana przez ciebie tempeeratura jest prawidłowa (spróbuj ją sparametryzować).
napisz funkcję która przekonwetyje temperaturę z Kelvinów na Celsjusze

2. Skonwertuj temperaturę z C -> F i z F -> C dokonaj sprawdzenia przekazywanej wartości

3. Skonwertuj temperaturę z K -> F i z F -> K dokonaj sprawdzenia przekazywanej wartości

4. Napisz funkcję odczytującą z konsoli dowolnie wprowadzony tekst i wyświetl go w konsoli weryfikując czy poprawnie został odczytany.
Skorzystaj z rozwiązań zaproponowanych przez Mkyong https://www.mkyong.com/java/how-to-read-input-from-console-java/  
(polecam rozwiązanie 2 i 3)
 
5. Napisz prosty kalkulator Srting, który przetworzy wprowadzone równanie np wprowadzając "10/2" otrzymasz wynik 5 w postaci cyfry.
Wprowadzając "1/4" otrzymasz wynik 0.25

6. Stwórz klasę w której zgromadzisz wszystkie elementy niezbedne do konwersji temperatury i uniemożliw ich zmianę w innej części kodu (nawet sobie) wykorzystując odpowiedni modyfikator. Umożliw dostęp do ich wartości tak aby można je było tylko odczytać.

7. Spróbuj odczytać jakikolwiek plik i wyświetlić jego zawartość na konsoli. Posłuż się przykładem ze strony Mkyong https://www.mkyong.com/java/java-how-to-read-a-file/  Wybierz najbardziej dogodną dla ciebie metodę.